***************
Getting Started
***************

To introduce ModelBuilder for ACE3P, here are some walkthroughs and examples that illustrate the basic features for generating ACE3P input files, running ACE3P simulations on computing resources at NERSC, and downloading the results for visualization and further analysis.

.. toctree::
    :maxdepth: 1

    installing.rst
    example-omega3p.rst
