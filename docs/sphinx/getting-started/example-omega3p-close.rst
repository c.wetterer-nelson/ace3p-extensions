Closing the Project
===================

.. rst-class:: step-number

18\. Close the Project

When you are done exploring the results, you can open the "ACE3P" menu and select the "Close Project" item to remove it from memory and clear most of the  view panels. For now, you also need to manually delete the ``pillbox-rtop4.ncdf`` item from the Pipeline Browser. You can do this by selecting that item in the Pipeline Browser and hitting the ``<Delete>`` key (or you can right-click and select the "Delete" item in the context menu).
