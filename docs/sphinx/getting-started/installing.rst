Installing ModelBuilder for ACE3P
=================================

The latest release packages for ModelBuilder for ACE3P are stored online in the `ModelBuilder for ACE3P Downloads Folder <https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/60aec5b32fa25629b9b6798b/>`_. Navigate a web browser to that link, locate the package file for your platform, and click the download button to the right of it.

.. image:: images/download-page.png
    :align: center

After the download is complete:

* For linux, extract the files from the ``.tar.gz`` package to a convenient folder on your system. The modelbuilder executable is in the ``bin`` folder.
* For macOS, open the ``.dmg`` package and drag the ``modelbuilder.app`` icon to a folder on your system. Because we don't distribute Modelbuilder for ACE3P through Apple's App Store, macOS will likely display warning dialogs when you unpack the ``.dmg`` file and start the application for the first time.
* For Windows, unzip the ``.zip`` package to a convenient folder. The modelbuilder executable is in the ``bin`` folder. If this is your first time installing ModelBuilder for ACE3P, you will also need to install OpenSSL on your machine, which requires Administrator priveleges. We made a video `Installing ModelBuilder-ACE3P on Windows <https://vimeo.com/558056351/a7b7a267a7>`_ with more details for installing on Windows.

When you first start ModelBuilder for ACE3P, it looks similar to ParaView, with the main differences being that (i) some of the ParaView toolbars have been hidden, and (ii) the left sidebar has two new dock widgets labeled "Resources" and "Attribute Editor", and (iii) there is a new "ACE3P" project menu.

.. image:: images/modelbuilder-start.png
    :align: center
