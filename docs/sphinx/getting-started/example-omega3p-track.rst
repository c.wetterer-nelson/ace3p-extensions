Tracking Job Status
===================

Use the "Simulation Jobs" panel to view this history of jobs submitted for the current project, and track the history of jobs that have not yet completed.


.. rst-class:: step-number

9\. Open Simulation Jobs Panel

After the export dialog closes, go to the "View" menu and enable the "Simulation Jobs" item. The panel will appear in the sidebar below the attribute editor. When the panel first appears, it will display one row with an entry for the job just submitted. Click the checkbox next to the "Polling" label to enable periodic polling of the job status. When the box is checked, ModelBuilder for ACE3P will poll NERSC and update the status column every 30 seconds until all jobs in the list reach a terminal state (either "complete" or "error"). The "Status" column will update from "created" to "queued" to "running" to "complete" as the status changes on the NERSC machine.

.. image:: images/project-jobs1.png
    :align: center


.. rst-class:: step-number

10\. Setup Simulation Jobs Panel

To see the full jobs panel, click its undock button (to the right of the "Simulation Jobs" label), drag the panel over the attribute panel, and drop it there so that it is displayed as another tab in the sidebar. Then click on the one row in the panel to open a details widget at the bottom of the panel.

.. image:: images/project-jobs2.png
    :align: center
