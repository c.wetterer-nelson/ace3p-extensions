Running Omega3P on NERSC
========================

You can submit jobs directly to the computing resources at NERSC from the ModelBuilder for ACE3P user interface. There is no need to open a terminal and use ``ssh`` or ``scp``.

.. rst-class:: step-number

7\. Login to NERSC

To submit jobs, first connect to a NERSC login node from ModelBuilder for ACE3P. To do this, go to the "ACE3P" menu and select the "Login to NERSC" item. ModelBuilder will display a dialog for entering your NERSC user name, password and Multi-Factor Authentication (MFA) string. Note that your credentials are passed directly to NERSC and are not stored in ModelBuilder. After you click the "OK" button, ModelBuilder will connect to NERSC, passing your credentials, and display a confirmation dialog.

.. image:: images/nersc-login.png
    :align: center


.. rst-class:: step-number

8\. Open the Export Dialog

| In the "ACE3P" menu, select the "Export Analysis..." item to open a dialog for generating the Omega3P input file. Find the checkbox labeled "Submit job to NERSC" and click on it to display additional fields for submitting the job. In this example, you only need to enter one field, the NERSC project repository, and use the default values for the other fields. (Be sure to scroll down and take note of what fields are available, including the number of nodes and cores to use.) After entering the repository field, click the "Apply" button to submit the job. It usually takes 5-10 seconds for the files to be uploaded to NERSC and a new job created. When the dialog closes, you can check the "Output Messages" view to verify that the processing was completed successfully.

.. image:: images/project-export.png
    :align: center
