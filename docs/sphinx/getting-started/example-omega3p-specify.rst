Specifying an Omega3P Analysis
==============================

You can use ModelBuilder for ACE3P to specify simulation inputs for all of the major codes in the ACE3P Suite: Omega3P, S3P, T3P, Track3P, TEM3P, and ACDTool. In this example, we will setup an Omega3P analysis to compute the first three resonant modes for the pillbox geometry.


.. rst-class:: step-number

3\. Set the Analysis Type to Omega3P

In the "Modules" tab, open the drowdown list to the right of the "Analysis" label and select "Omege3P". In response, ModelBuilder will open additonal tabs to the right of "Modules". Resize the sidebar panel larger to see the "Boundary Conditions", "Materials", and "Analysis" tabs.


.. rst-class:: step-number

4\. Assign Boundary Conditions

Click on the "Boundary Conditions" tab to see a two-column list of the side sets in the model. In the right-hand column, set the surface type to "Magnetic" for side set ID 1 and 2, and "Exterior" for side set ID 6. (Double-click the "Please Select" text to open a drop down list, then select from this list, then hit <Enter> to confirm the selection.) When you are done, the tab should look like this.

.. image:: images/project-bcs.png
    :align: center



.. rst-class:: step-number

5\. Specify the Number of Eigenmodes

Next click on the "Analysis" tab, find the field labeled "Number of eigenmodes searched", and change the value from 1 to 3.


.. rst-class:: step-number

6\. Save the Project

At this point, we have entered enough information for an Omega3P analysis, so go to the "ACE3P" menu and select the "Save Project" item.
