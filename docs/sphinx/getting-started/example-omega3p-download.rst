Downloading Omega3P Results
===========================

The contents of the job folder can be downloaded from the NERSC machine to the local file system for visualization and local postprocessing. (A future version of ModelBuilder for ACE3P will support remote visualization so that downloading won't be required.)

.. rst-class:: step-number

11\. Download Job Directory

Once the job is complete, you can download the job directory on NERSC including the ``omega3p_results`` subdirectory. Make sure the first (only) row in the jobs table is selected, then in the details section below, find the download toolbar to the right of the "Remote Directory" field. Click on that button to start the download process. The files will be downloaded from NERSC to the path shown in the "Download Directory" field.

.. image:: images/download-toolbutton-highlight.png
    :align: center
