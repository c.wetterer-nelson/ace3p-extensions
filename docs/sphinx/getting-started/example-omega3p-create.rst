Creating an ACE3P Project
=========================

.. figure:: images/ace3p-menu.png
    :figwidth: 40%
    :align: right

Most of your menu interactions will be with the "ACE3P" project menu, which has items to open existing ModelBuilder projects, create new projects, save projects, and close projects. It also includes a menu item "Export Analysis..." to generate an ACE3P input file from your project, and "Login to NERSC" which enables you to  submit ACE3P simulation jobs to computing resources at NERSC.

To provide a working example, we will be using the ``pillbox-rtop4.gen`` mesh file from the ACE3P code workshops. You can download a copy of this file from the `ModelBuilder for ACE3P Data Folder <https://data.kitware.com/#collection/58fa68228d777f16d01e03e5/folder/60aef5cf2fa25629b9b69632/>`_ if needed.

.. Apply class for custom formatting of numbered steps.
.. rst-class:: step-number

1\. Specify a New Project Directory

To begin, open the "ACE3P" menu and select the "New Project..." item. In response, ModelBuilder displays a file system dialog for selecting a project directory that will be used for storing persistent data. Standard practice is to create a new directory, but you can also use an existing directory. But if you select an existing directory that contains any files or subdirectories, ModelBuilder will prompt you to confirm that it is OK to delete the contents of the directory before proceeding.


.. rst-class:: step-number

2\. Input the Analysis Mesh

Once the directory is selected, ModelBuilder then displays a dialog to complete the new project specification. In the field labeled "Analysis Mesh", enter the path to the ``pillbox-rtop4.gen`` file on your local file system and click the "Apply" button.

.. figure:: images/new-project-dialog.png
    :figwidth: 50%
    :align: center

ModelBuilder will then close the dialog and create the new project. The pillbox mesh will be displayed in the 3-D view and the "Attribute Editor" tab will contain a child tab labeled "Modules".

.. image:: images/project-created.png
    :align: center
