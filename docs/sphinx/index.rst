######################
ModelBuilder for ACE3P
######################

ModelBuilder for ACE3P is a customized version of the `CMB <https://computationalmodelbuilder.org/>`_ ModelBuilder simulation preprocessing application for use with the `ACE3P software suite <https://slac.stanfore.edu/>`_ for particle accelerator simulation. ModelBuilder for ACE3P provides a graphical user interface for loading mesh files (Genesis, NetCDF) and specifying inputs for one or more of the ACE3P codes: Omega3P, S3P, T3P, Track3P, TEM3P, and ACDTool. Simulation jobs can be submitted from ModelBuilder for ACE3P to the computing machines at the `NERSC <https://www.nersc.gov/>`_ computing facility.

Contents:

.. toctree::
   :maxdepth: 2

   getting-started/index.rst

.. image:: ./splashImage_small.png

..
    ##################
    Indices and tables
    ##################

    * :ref:`genindex`
    * :ref:`modindex`
    * :ref:`search`
