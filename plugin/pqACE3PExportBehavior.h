//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_plugin_pqACE3PExportBehavior_h
#define smtk_simulation_ace3p_plugin_pqACE3PExportBehavior_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/operation/Operation.h"

#include "pqReaction.h"

#include <QObject>

#include <string>

/// A reaction for writing a resource manager's state to disk.
class pqACE3PExportReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqACE3PExportReaction(QAction* parent);

protected:
  /**
  * Called when the action is triggered.
  */
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqACE3PExportReaction)
};

/** \brief Add a menu item for writing the state of the resource manager.
  */
class pqACE3PExportBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqACE3PExportBehavior* instance(QObject* parent = nullptr);
  ~pqACE3PExportBehavior() override;

  void exportProject();

signals:
  void jobAdded(const QString& cumulusJobId);

protected slots:
  void onOperationExecuted(const smtk::operation::Operation::Result& result);

protected:
  pqACE3PExportBehavior(QObject* parent = nullptr);

  // Asks user to save project if modified
  bool cleanProject(smtk::project::ProjectPtr project);

private:
  Q_DISABLE_COPY(pqACE3PExportBehavior);
};

#endif
