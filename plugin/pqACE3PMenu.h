//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PMenu_h
#define pqACE3PMenu_h

#include "smtk/PublicPointerDefs.h"

#include <QActionGroup>

class QAction;

class pqACE3PRecentProjectsMenu;

/** \brief Adds the "ACE3P" menu to the application main window
  */
class pqACE3PMenu : public QActionGroup
{
  Q_OBJECT
  using Superclass = QActionGroup;

public slots:
  void onProjectOpened(smtk::project::ProjectPtr project);
  void onProjectClosed();

public:
  pqACE3PMenu(QObject* parent = nullptr);
  ~pqACE3PMenu() override;

  bool startup();
  void shutdown();

private:
  QAction* m_nerscBrowserAction;
  QAction* m_nerscLoginAction;
  QAction* m_newProjectAction;
  QAction* m_exportProjectAction;
  QAction* m_saveProjectAction;
  QAction* m_openProjectAction;
  QAction* m_closeProjectAction;

  QAction* m_recentProjectsAction;

  pqACE3PRecentProjectsMenu* m_recentProjectsMenu;
  bool m_saveMenuConfigured = false;

  Q_DISABLE_COPY(pqACE3PMenu);
};

#endif // pqACE3PMenu_h
