//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_plugin_pqACE3PNewAnalysisBehavior_h
#define smtk_simulation_ace3p_plugin_pqACE3PNewAnalysisBehavior_h

#include "smtk/PublicPointerDefs.h"
#include "smtk/operation/Operation.h"

#include <QObject>

/// A reaction for calling the NewAnalysis operation on the open project.
class pqACE3PNewAnalysisReaction : public QObject
{
  Q_OBJECT

public:
  /**
  * Constructor. Parent cannot be NULL.
  */
  pqACE3PNewAnalysisReaction(QObject* parent);

  void createNewAnalysis();

public slots:
  void onTriggered() { this->createNewAnalysis(); }

private:
  Q_DISABLE_COPY(pqACE3PNewAnalysisReaction)
};

#endif
