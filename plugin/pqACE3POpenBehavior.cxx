//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqACE3POpenBehavior.h"

#include "pqACE3PLoader.h"

// ParaView
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqServer.h"
#include "pqWaitCursor.h"
#include "vtkSMProperty.h"
#include "vtkSMProxy.h"
#include "vtkSMSessionProxyManager.h"
#include "vtkSMStringVectorProperty.h"

// Qt
#include <QAction>
#include <QDebug>
#include <QDialog>
#include <QDir>
#include <QMessageBox>

//-----------------------------------------------------------------------------
pqACE3POpenReaction::pqACE3POpenReaction(QAction* parentObject)
  : Superclass(parentObject)
{
}

//-----------------------------------------------------------------------------
void pqACE3POpenReaction::openProject()
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();

  // Check settings for ProjectsRootFolder
  QString workspaceFolder;
  vtkSMStringVectorProperty* workspaceStringProp = nullptr;
  vtkSMProxy* proxy = server->proxyManager()->GetProxy("settings", "SMTKSettings");
  if (proxy)
  {
    vtkSMProperty* workspaceProp = proxy->GetProperty("ProjectsRootFolder");
    workspaceStringProp = vtkSMStringVectorProperty::SafeDownCast(workspaceProp);
    if (workspaceStringProp != nullptr)
    {
      workspaceFolder = workspaceStringProp->GetElement(0);
    } // if (projectStringProp)
  }

  // Construct a file dialog for the user to select the project directory to load
  pqFileDialog fileDialog(
    server, pqCoreUtilities::mainWidget(), tr("Select Project (Directory):"), workspaceFolder);
  fileDialog.setObjectName("FileOpenDialog");
  fileDialog.setFileMode(pqFileDialog::Directory);
  fileDialog.setShowHidden(true);
  if (fileDialog.exec() != QDialog::Accepted)
  {
    return;
  }
  auto selectedFiles = fileDialog.getSelectedFiles();
  QString directory = selectedFiles[0];
  pqWaitCursor cursor;
  if (pqACE3PLoader::instance()->load(server, directory) && (workspaceStringProp != nullptr))
  {
    // Update workspaceStringProp
    QDir dir(directory);
    if (dir.cdUp())
    {
      workspaceStringProp->SetElement(0, dir.absolutePath().toStdString().c_str());
    }
  }
} // openProject()

//-----------------------------------------------------------------------------
static pqACE3POpenBehavior* g_instance = nullptr;

pqACE3POpenBehavior::pqACE3POpenBehavior(QObject* parent)
  : Superclass(parent)
{
}

pqACE3POpenBehavior* pqACE3POpenBehavior::instance(QObject* parent)
{
  if (!g_instance)
  {
    g_instance = new pqACE3POpenBehavior(parent);
  }

  if (g_instance->parent() == nullptr && parent)
  {
    g_instance->setParent(parent);
  }

  return g_instance;
}

pqACE3POpenBehavior::~pqACE3POpenBehavior()
{
  if (g_instance == this)
  {
    g_instance = nullptr;
  }

  QObject::disconnect(this);
}
