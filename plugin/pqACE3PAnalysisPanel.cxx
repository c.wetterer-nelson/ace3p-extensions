//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqACE3PAnalysisPanel.h"
#include "pqACE3PNewAnalysisReaction.h"

#include "pqActiveObjects.h"

#include "plugin/pqACE3PExportBehavior.h"
#include "plugin/pqACE3PLoader.h"
#include "plugin/pqACE3PNewBehavior.h"
#include "smtk/simulation/ace3p/qt/qtAnalysisWidget.h"
#include "smtk/simulation/ace3p/qt/qtJobsDetails.h"

#include "pqApplicationCore.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"

#include <QDebug>

pqACE3PAnalysisPanel::pqACE3PAnalysisPanel(QWidget* parent)
  : QDockWidget(parent)
  , m_mainWidget(nullptr)
  , m_jobDetails(nullptr)
{
  this->setObjectName("Jobs Panel");
  this->setWindowTitle("Simulation Jobs");

  QWidget* topWidget = new QWidget(this);

  m_mainWidget = new smtk::simulation::ace3p::qtAnalysisWidget(parent);
  m_mainWidget->setObjectName("analysis_panel_widget");

  m_jobDetails = new smtk::simulation::ace3p::qtJobsDetails(parent);
  m_jobDetails->setObjectName("jobs_details_widget");

  QVBoxLayout* layout = new QVBoxLayout;
  layout->addWidget(m_mainWidget);
  layout->setStretch(0, 5);
  layout->addWidget(m_jobDetails);
  layout->setStretch(1, 1);
  topWidget->setLayout(layout);

  this->setWidget(topWidget);

  auto pqCore = pqApplicationCore::instance();
  if (!pqCore)
  {
    qWarning() << "pqACE3PAnalysisPanel missing pqApplicationCore";
    return;
  }

  auto smtkBehavior = pqSMTKBehavior::instance();
  // Now listen for future connections.
  QObject::connect(
    smtkBehavior,
    SIGNAL(removingManagerFromServer(pqSMTKWrapper*, pqServer*)),
    this,
    SLOT(sourceRemoved(pqSMTKWrapper*, pqServer*)));

  auto creater = pqACE3PNewBehavior::instance();
  QObject::connect(
    creater,
    &pqACE3PNewBehavior::projectCreated,
    m_mainWidget,
    &smtk::simulation::ace3p::qtAnalysisWidget::onProjectLoaded);
  QObject::connect(
    creater,
    &pqACE3PNewBehavior::projectCreated,
    m_jobDetails,
    &smtk::simulation::ace3p::qtJobsDetails::setProject);

  auto loader = pqACE3PLoader::instance();
  QObject::connect(
    loader,
    &pqACE3PLoader::projectOpened,
    m_mainWidget,
    &smtk::simulation::ace3p::qtAnalysisWidget::onProjectLoaded);
  QObject::connect(
    loader,
    &pqACE3PLoader::projectOpened,
    m_jobDetails,
    &smtk::simulation::ace3p::qtJobsDetails::setProject);

  auto activeObjects = &pqActiveObjects::instance();
  QObject::connect(
    activeObjects,
    &pqActiveObjects::dataUpdated,
    m_mainWidget,
    &smtk::simulation::ace3p::qtAnalysisWidget::onAnalysesUpdated);

  auto exporter = pqACE3PExportBehavior::instance();
  QObject::connect(
    exporter,
    &pqACE3PExportBehavior::jobsUpdated,
    m_mainWidget,
    &smtk::simulation::ace3p::qtAnalysisWidget::onJobsUpdated);

  // connect new analysis reaction
  auto newAnalysisReaction = new pqACE3PNewAnalysisReaction(this);
  QObject::connect(
    m_mainWidget,
    &smtk::simulation::ace3p::qtAnalysisWidget::requestNewAnalysis,
    newAnalysisReaction,
    &pqACE3PNewAnalysisReaction::onTriggered);
}

pqACE3PAnalysisPanel::~pqACE3PAnalysisPanel()
{
  auto pqCore = pqApplicationCore::instance();
  pqCore->unRegisterManager(QString("analysis_panel"));
}

void pqACE3PAnalysisPanel::infoSlot(const QString& msg)
{
  // Would like to emit signal to main window status bar
  // but that is not currently working. Instead, qInfo()
  // messages are sent to the CMB Log Window:
  qInfo() << "AnalysisPanel:" << msg;
}

void pqACE3PAnalysisPanel::sourceRemoved(pqSMTKWrapper* mgr, pqServer* server)
{
  if (!mgr || !server)
  {
    return;
  }
}
