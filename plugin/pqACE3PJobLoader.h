//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef pqACE3PJobLoader_h
#define pqACE3PJobLoader_h

#include <QObject>

class QDir;
class pqPipelineSource;
class pqView;

namespace smtk
{
namespace simulation
{
namespace ace3p
{
class qtModeSelectDialog;
} // namespace ace3p
} // namespace simulation
} // namespace smtk

class pqACE3PJobLoader : public QObject
{
  Q_OBJECT
  typedef QObject Superclass;

public:
  // @brief return an instance of the loader
  static pqACE3PJobLoader* instance(QObject* parent = nullptr);

  // @brief destructor
  ~pqACE3PJobLoader();

  // @brief convenience function for destroying a pipeline
  // @note reimplemented from SLACTools
  static void destroyPipelineSourceAndConsumers(pqPipelineSource* source);
public slots:
  // @brief open a new view and load the results in the JobDir
  void setupPipeline(const QString& jobDir, const QStringList& modeFiles);

  // @brief open a dialog for selecting mode files from the JobDir
  void openModeSelectDialog(const QString& jobDir);

protected:
  // @brief protected constructor
  pqACE3PJobLoader(QObject* parent = nullptr);

  // @brief find the download folder in a job directory
  QDir findDownloadFolder(const QString& jobDir);

  // @brief find a pqView or create one if needed
  pqView* findView(const pqPipelineSource* source);

  // @brief find a pipeline source. reimplemented from pqSLACManager
  pqPipelineSource* findPipelineSource(const char* SMName);

  // @brief get the filepath of the mesh file
  QStringList getMeshFileName(const QString& jobDir);

  // @brief get the mode files from a job
  QStringList getModeFileNames(const QString& jobDir);

  // holds on to the popup dialog for selecting mode files
  smtk::simulation::ace3p::qtModeSelectDialog* m_dialog;

private:
  Q_DISABLE_COPY(pqACE3PJobLoader);
};
#endif
