//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqACE3PJobsPanel.h"
#include "pqACE3PJobLoader.h"

#include "pqActiveObjects.h"
#include "pqApplicationCore.h"

#include "plugin/pqACE3PCloseBehavior.h"
#include "plugin/pqACE3PExportBehavior.h"
#include "plugin/pqACE3PLoader.h"
#include "plugin/pqACE3PNewBehavior.h"
#include "plugin/pqNerscFileBrowserBehavior.h"

#include "smtk/newt/qtNewtFileBrowserWidget.h"
#include "smtk/simulation/ace3p/qt/qtJobsWidget.h"

#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"

#include <QDebug>

pqACE3PJobsPanel::pqACE3PJobsPanel(QWidget* parent)
  : QDockWidget(parent)
  , m_jobWidget(nullptr)
{
  this->setObjectName("Jobs Panel");
  this->setWindowTitle("Simulation Jobs");

  m_jobWidget = new smtk::simulation::ace3p::qtJobsWidget(parent);
  m_jobWidget->setObjectName("jobs_panel_widget");

  this->setWidget(m_jobWidget);

  auto pqCore = pqApplicationCore::instance();
  if (!pqCore)
  {
    qWarning() << "pqACE3PJobsPanel missing pqApplicationCore";
    return;
  }

  auto smtkBehavior = pqSMTKBehavior::instance();
  // Now listen for future connections.
  QObject::connect(
    smtkBehavior,
    SIGNAL(removingManagerFromServer(pqSMTKWrapper*, pqServer*)),
    this,
    SLOT(sourceRemoved(pqSMTKWrapper*, pqServer*)));

  auto creater = pqACE3PNewBehavior::instance();
  QObject::connect(
    creater,
    &pqACE3PNewBehavior::projectCreated,
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::setProject);

  auto loader = pqACE3PLoader::instance();
  QObject::connect(
    loader,
    &pqACE3PLoader::projectOpened,
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::setProject);

  auto exporter = pqACE3PExportBehavior::instance();

  auto closer = pqACE3PCloseBehavior::instance();
  QObject::connect(
    closer,
    &pqACE3PCloseBehavior::projectClosed,
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::onProjectClosed);
  QObject::connect(
    exporter,
    &pqACE3PExportBehavior::jobAdded,
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::onJobAdded);

  // connect navigation buttons to nersc file browser
  auto nerscBrowserInstance = pqNerscFileBrowserBehavior::instance();
  auto nerscBrowserWidget = nerscBrowserInstance->getBrowser();
  QObject::connect(
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::requestNavigateDir,
    nerscBrowserWidget,
    &newt::qtNewtFileBrowserWidget::onRequestNavigate);

  auto jobLoader = pqACE3PJobLoader::instance();
  QObject::connect(
    m_jobWidget,
    &smtk::simulation::ace3p::qtJobsWidget::requestLoadJob,
    jobLoader,
    &pqACE3PJobLoader::openModeSelectDialog);
}

pqACE3PJobsPanel::~pqACE3PJobsPanel()
{
  auto pqCore = pqApplicationCore::instance();
  pqCore->unRegisterManager(QString("analysis_panel"));
}

void pqACE3PJobsPanel::infoSlot(const QString& msg)
{
  // Would like to emit signal to main window status bar
  // but that is not currently working. Instead, qInfo()
  // messages are sent to the CMB Log Window:
  qInfo() << "pqACE3PJobsPanel:" << msg;
}

void pqACE3PJobsPanel::sourceRemoved(pqSMTKWrapper* mgr, pqServer* server)
{
  if (!mgr || !server)
  {
    return;
  }
}
