//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_plugin_pqACE3PNewBehavior_h
#define smtk_simulation_ace3p_plugin_pqACE3PNewBehavior_h

#include "smtk/PublicPointerDefs.h"

#include "pqReaction.h"

#include <QObject>

class pqACE3PNewBehaviorInternals;

/// \brief A reaction for creating a new ACE3P project.
class pqACE3PNewReaction : public pqReaction
{
  Q_OBJECT
  typedef pqReaction Superclass;

public:
  pqACE3PNewReaction(QAction* parent);
  ~pqACE3PNewReaction() = default;

protected:
  /// Called when the action is triggered.
  void onTriggered() override;

private:
  Q_DISABLE_COPY(pqACE3PNewReaction)
};

/// \brief Creates new ACE3P project.
class pqACE3PNewBehavior : public QObject
{
  Q_OBJECT
  using Superclass = QObject;

public:
  static pqACE3PNewBehavior* instance(QObject* parent = nullptr);
  ~pqACE3PNewBehavior() override;

  void newProject();

signals:
  void projectCreated(smtk::project::ProjectPtr);

protected:
  pqACE3PNewBehavior(QObject* parent = nullptr);

private:
  pqACE3PNewBehaviorInternals* Internals;

  Q_DISABLE_COPY(pqACE3PNewBehavior);
};

#endif
