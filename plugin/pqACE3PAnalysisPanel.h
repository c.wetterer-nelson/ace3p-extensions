//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME pqACE3PAnalysisPanel - display jobs on remote system
// .SECTION Description

#ifndef plugin_pqACE3PAnalysisPanels_h
#define plugin_pqACE3PAnalysisPanels_h

#include "smtk/extension/qt/qtUIManager.h"

#include <QDockWidget>

class pqSMTKWrapper;
class pqServer;

namespace smtk
{
namespace simulation
{
namespace ace3p
{
class qtAnalysisWidget;
class qtJobsWidget;
} // namespace ace3p
} // namespace simulation
} // namespace smtk

class QTableView;

class pqACE3PAnalysisPanel : public QDockWidget
{
  Q_OBJECT

public:
  pqACE3PAnalysisPanel(QWidget* parent);
  virtual ~pqACE3PAnalysisPanel();

signals:
  void projectUpdated(smtk::project::ProjectPtr);
public slots:

protected slots:
  void infoSlot(const QString& msg);

  virtual void sourceRemoved(pqSMTKWrapper* mgr, pqServer* server);

protected:
  smtk::simulation::ace3p::qtAnalysisWidget* m_mainWidget;
  smtk::simulation::ace3p::qtJobsWidget* m_jobWidget;
};

#endif // __ACE3PAnalysisPanels_h
