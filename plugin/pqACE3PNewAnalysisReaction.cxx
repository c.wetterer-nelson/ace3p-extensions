//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "pqACE3PNewAnalysisReaction.h"

#include "smtk/simulation/ace3p/operations/NewAnalysis.h"
#include "smtk/simulation/ace3p/qt/qtProjectRuntime.h"
#include "smtk/simulation/ace3p/utility/AttributeUtils.h"

// SMTK
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKBehavior.h"
#include "smtk/extension/paraview/appcomponents/pqSMTKWrapper.h"
#include "smtk/extension/qt/qtOperationDialog.h"
#include "smtk/io/Logger.h"
#include "smtk/operation/Manager.h"
#include "smtk/project/Project.h"
#include "smtk/project/ResourceContainer.h"

// Paraview client
#include "pqActiveObjects.h"
#include "pqCoreUtilities.h"
#include "pqFileDialog.h"
#include "pqPresetDialog.h"
#include "pqServer.h"

// Qt
#include <QAction>
#include <QDebug>
#include <QGridLayout>
#include <QMessageBox>
#include <QPushButton>
#include <QSharedPointer>
#include <QSpacerItem>
#include <QString>
#include <QTextStream>
#include <QtGlobal>

#include <cassert>

//-----------------------------------------------------------------------------
pqACE3PNewAnalysisReaction::pqACE3PNewAnalysisReaction(QObject* parentObject)
{
  this->setParent(parentObject);
}
//-----------------------------------------------------------------------------
void pqACE3PNewAnalysisReaction::createNewAnalysis()
{
  // Access the active server
  pqServer* server = pqActiveObjects::instance().activeServer();
  pqSMTKWrapper* wrapper = pqSMTKBehavior::instance()->resourceManagerForServer(server);
  auto opManager = wrapper->smtkOperationManager();

  // Instantiate the NewAnalysis operator
  auto newAnalysisOp = opManager->create<smtk::simulation::ace3p::NewAnalysis>();
  assert(newAnalysisOp != nullptr);

  // connect to the current project
  auto project = smtk::simulation::ace3p::qtProjectRuntime::instance()->project();
  newAnalysisOp->parameters()->associate(project);

  // Construct a modal dialog for the operation spec
  QSharedPointer<smtk::extension::qtOperationDialog> dialog =
    QSharedPointer<smtk::extension::qtOperationDialog>(new smtk::extension::qtOperationDialog(
      newAnalysisOp,
      wrapper->smtkResourceManager(),
      wrapper->smtkViewManager(),
      pqCoreUtilities::mainWidget()));
  dialog->setObjectName("NewAnalysisACE3PDialog");
  dialog->setWindowTitle("Specify New Analysis Properties");
  dialog->exec();
}
