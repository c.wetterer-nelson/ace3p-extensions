//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtNewtFileBrowserDialog.h
// .SECTION Description
// .SECTION See Also

#ifndef smtk_newt_qtNewtFileBrowserDialog
#define smtk_newt_qtNewtFileBrowserDialog

#include "smtk/newt/Exports.h"

#include <QDialog>

class QDialogButtonBox;

namespace newt
{

class qtNewtFileBrowserWidget;

/** \brief Simple wrapper for qtNewFileBrowserWidget
 *
 */

class SMTKNEWT_EXPORT qtNewtFileBrowserDialog : public QDialog
{
  Q_OBJECT

public:
  qtNewtFileBrowserDialog(QWidget* parentWidget = nullptr);
  ~qtNewtFileBrowserDialog() = default;

signals:
  // Emitted when user clicks the dialog's "Apply" button.
  void applyPath(const QString& path);

  // Emitted when user clicks the widget's "Copy" button.
  void pathCopied(const QString& path);

private:
  qtNewtFileBrowserWidget* m_widget = nullptr;
  QDialogButtonBox* m_buttonBox = nullptr;
};
} // namespace newt

#endif
