//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
// .NAME qtNewtFileBrowserWidget.h
// .SECTION Description
// .SECTION See Also

#ifndef smtk_newt_qtNewtFileBrowserWidget
#define smtk_newt_qtNewtFileBrowserWidget

#include "smtk/newt/Exports.h"

#include <QIcon>
#include <QModelIndex>
#include <QStandardItemModel>
#include <QString>
#include <QWidget>

class QContextMenuEvent;
class QNetworkReply;

namespace Ui
{
class qtNewtFileBrowserWidget;
}

namespace newt
{
class qtNewtInterface;

class SMTKNEWT_EXPORT qtNewtFileBrowserWidget : public QWidget
{
  Q_OBJECT

public:
  qtNewtFileBrowserWidget(QWidget* parentWidget = nullptr);
  ~qtNewtFileBrowserWidget();

  const QString& currentPath() const { return m_currentPath; }

signals:
  void beginGotoPath(const QString& path);
  void endGotoPath(const QString& path);

  // Emitted when user clicks "Copy" button
  void pathCopied(const QString& path);

public slots:
  void onRequestNavigate(const QString& path);
protected slots:
  void onItemActivated(const QModelIndex& index);
  void onListFolderReply();
  void onLogin(const QString& username);
  void onNewtError(QString, QNetworkReply* reply = nullptr);
  void onHomeClicked();
  void onScratchClicked();

protected:
  void contextMenuEvent(QContextMenuEvent* event);
  bool getCommandReply(QNetworkReply* reply, QString& text);
  void gotoPath(const QString& path);
  void goUpDirectory();
  void handleNetworkError(QNetworkReply* reply);
  void viewFile(const QString& name);

  QString m_currentPath; // current display
  QString m_homePath;    // user home directory
  QString m_requestPath; // requested path
  QString m_scratchPath; // user scratch directory

  QIcon m_fileIcon;
  QIcon m_folderIcon;

  QStandardItemModel m_model;
  qtNewtInterface* m_newt;

private:
  Ui::qtNewtFileBrowserWidget* ui;
};

} // namespace newt

#endif
