include_directories(${CMAKE_CURRENT_BINARY_DIR} ${CMAKE_CURRENT_SOURCE_DIR})

set(ui_files
  qtNewtFileBrowserWidget.ui
  qtNewtLoginDialog.ui
)

set(src
  qtApplicationLoginSupport.cxx
  qtNewtFileBrowserDialog.cxx
  qtNewtFileBrowserWidget.cxx
  qtNewtInterface.cxx
  qtNewtLoginDialog.cxx
)

#add moc files
set(mochdrs
  qtApplicationLoginSupport.h
  qtNewtFileBrowserDialog.h
  qtNewtFileBrowserWidget.h
  qtNewtInterface.h
  qtNewtLoginDialog.h
)

set(CMAKE_AUTOUIC 1)
set(CMAKE_AUTOMOC 1)
set(CMAKE_AUTORCC 1)

#install headers
set(hdrs
  qtNewtInterface.h
  qtNewtLoginDialog.h
)

#extension library
add_library(smtkNewt
 ${src}
 ${hdrs}
 ${mochdrs}
 ${ui_files}
 qtNewtFileBrowserIcons.qrc
)
smtk_public_headers(smtkNewt ${hdrs})

#we need to add the location of the moc files to the include dir
target_include_directories(smtkNewt PRIVATE ${CMAKE_CURRENT_BINARY_DIR})
target_include_directories(smtkNewt PUBLIC
  $<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<INSTALL_INTERFACE:include>
)

#publicly link to smtkCore
target_link_libraries(smtkNewt LINK_PUBLIC
  smtkCore
  Qt5::Core
  Qt5::Gui
  Qt5::Network
  Qt5::Widgets
)
smtk_export_header(smtkNewt Exports.h)

#install the library and exports
smtk_install_library(smtkNewt)

if (BUILD_EXAMPLES)
  add_subdirectory(examples)
endif ()
