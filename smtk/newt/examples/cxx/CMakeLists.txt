set(examples
  qtNerscFileBrowser
  )

foreach(example ${examples})
  add_executable(${example} MACOSX_BUNDLE
    ${example}.cxx
    )
  target_link_libraries(${example}
    smtkQtExt
    smtkCore
    smtkNewt
    )
endforeach(example)
