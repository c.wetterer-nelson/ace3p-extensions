//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#ifndef pybind_smtk_simulation_ace3p_Project_h
#define pybind_smtk_simulation_ace3p_Project_h

#include <pybind11/pybind11.h>

#include "smtk/simulation/ace3p/Project.h"

#include "smtk/simulation/ace3p/JobsManifest.h"

#include "smtk/attribute/Attribute.h"

namespace py = pybind11;

inline PySharedPtrClass<smtk::simulation::ace3p::Project>
pybind11_init_smtk_simulation_ace3p_Project(py::module& m)
{
  PySharedPtrClass<smtk::simulation::ace3p::Project, smtk::project::Project> instance(m, "Project");
  instance
    .def_static(
      "create",
      (std::shared_ptr<smtk::simulation::ace3p::Project>(*)()) &
        smtk::simulation::ace3p::Project::create)
    .def("setJobRecordField", &smtk::simulation::ace3p::Project::setJobRecordField)
    // .def("getJobRecordField", &smtk::simulation::ace3p::Project::getJobRecordField)
    .def("readJobsManifest", &smtk::simulation::ace3p::Project::readJobsManifest)
    .def("jobsManifest", &smtk::simulation::ace3p::Project::jobsManifest)
    .def("jobData", &smtk::simulation::ace3p::Project::jobData)
    .def("onJobSubmit", &smtk::simulation::ace3p::Project::onJobSubmit);
  return instance;
}

#endif
