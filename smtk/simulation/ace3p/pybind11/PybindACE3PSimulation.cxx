//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include <pybind11/pybind11.h>

namespace py = pybind11;

template<typename T, typename... Args>
using PySharedPtrClass = py::class_<T, std::shared_ptr<T>, Args...>;

#include "PybindJobsManifest.h"
#include "PybindMetadata.h"
#include "PybindProject.h"
#include "PybindRegistrar.h"

// PYBIND11_DECLARE_HOLDER_TYPE(T, std::shared_ptr<T>);

PYBIND11_MODULE(_smtkPybindACE3PSimulation, m)
{
  m.doc() = "<description>";
  py::module smtk = m.def_submodule("smtk", "<description>");
  py::module ace3p = smtk.def_submodule("ace3p", "<description>");

  py::class_<smtk::simulation::ace3p::JobsManifest> smtk_ace3p_JobsManifest =
    pybind11_init_smtk_simulation_ace3p_JobsManifest(ace3p);
  py::class_<smtk::simulation::ace3p::Metadata> smtk_ace3p_Metadata =
    pybind11_init_smtk_simulation_ace3p_Metadata(ace3p);
  py::class_<smtk::simulation::ace3p::Registrar> smtk_ace3p_Registrar =
    pybind11_init_smtk_simulation_ace3p_Registrar(ace3p);

  PySharedPtrClass<smtk::project::Project> smtk_simulation_ace3p_Project =
    pybind11_init_smtk_simulation_ace3p_Project(ace3p);
}
