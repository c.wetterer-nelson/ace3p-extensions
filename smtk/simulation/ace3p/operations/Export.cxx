//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#include "smtk/simulation/ace3p/operations/Export.h"

#include "smtk/simulation/ace3p/Metadata.h"
#include "smtk/simulation/ace3p/Project.h"

// SMTK includes
#include "smtk/attribute/Attribute.h"
#include "smtk/attribute/Definition.h"
#include "smtk/attribute/DirectoryItem.h"
#include "smtk/attribute/FileItem.h"
#include "smtk/attribute/IntItem.h"
#include "smtk/attribute/Item.h"
#include "smtk/attribute/ItemDefinition.h"
#include "smtk/attribute/ReferenceItem.h"
#include "smtk/attribute/Resource.h"
#include "smtk/attribute/ResourceItem.h"
#include "smtk/attribute/SearchStyle.h"
#include "smtk/attribute/StringItem.h"
#include "smtk/io/Logger.h"
#include "smtk/model/Resource.h"
#include "smtk/operation/Manager.h"
#include "smtk/operation/operators/ImportPythonOperation.h"
#include "smtk/project/Project.h"

// Build dir includes
#include "smtk/simulation/ace3p/Export_xml.h"

#include <boost/filesystem.hpp>

#include <iostream>
#include <set>
#include <string>
#include <vector>

namespace
{
const int OP_SUCCEEDED = static_cast<int>(smtk::operation::Operation::Outcome::SUCCEEDED);

// Macro for checking condition and returning
#define checkMacro(condition, msg)                                                                 \
  do                                                                                               \
  {                                                                                                \
    if (!(condition))                                                                              \
    {                                                                                              \
      smtkErrorMacro(this->log(), msg);                                                            \
      return this->createResult(smtk::operation::Operation::Outcome::FAILED);                      \
    }                                                                                              \
  } while (0)
} // namespace

namespace smtk
{
namespace simulation
{
namespace ace3p
{

smtk::operation::Operation::Result Export::operateInternal()
{
  auto& logger = this->log();

  // Make sure the python operation (ACE3P.py) exists.
  if (Metadata::WORKFLOWS_DIRECTORY.empty())
  {
    smtkErrorMacro(logger, "Internal Error: Metadata::WORKFLOWS_DIRECTORY not defined.");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  boost::filesystem::path workflowDir(Metadata::WORKFLOWS_DIRECTORY);
  boost::filesystem::path pyPath = workflowDir / "ACE3P.py";
  if (!boost::filesystem::exists(pyPath))
  {
    smtkErrorMacro(logger, "Error: expecing python file at " << pyPath.string());
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  // Import the python op
  smtk::operation::ImportPythonOperation::Ptr importPythonOp =
    this->manager()->create<smtk::operation::ImportPythonOperation>();
  if (!importPythonOp)
  {
    smtkErrorMacro(logger, "Unable to create ImportPythonOperation");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }
  importPythonOp->parameters()->findFile("filename")->setValue(pyPath.string());
  auto loadResult = importPythonOp->operate();
  if (loadResult->findInt("outcome")->value() != OP_SUCCEEDED)
  {
    smtkErrorMacro(logger, "Failed to load export script " << pyPath.string());
    // Copy the outcome
    int loadOutcome = loadResult->findInt("outcome")->value();
    auto thisOutcome = static_cast<smtk::operation::Operation::Outcome>(loadOutcome);
    return this->createResult(thisOutcome);
  }

  // Access the unique name assigned to the operation
  std::string operationName = loadResult->findString("unique_name")->value();

  // Instantiate the operation
  smtk::operation::Operation::Ptr pythonOp = this->manager()->create(operationName);
  if (pythonOp == nullptr)
  {
    smtkErrorMacro(logger, "Failed to instantate operation from export script ");
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  // Configure the operation
  auto refItem = this->parameters()->associations();
  auto project = refItem->valueAs<smtk::simulation::ace3p::Project>();

  // Copy POD parameters to the python operation
  std::vector<std::string> itemNames = { "OutputFolder", "OutputFilePrefix", "MeshFile" };
  for (auto name : itemNames)
  {
    smtk::attribute::ConstItemPtr sourceItem =
      this->parameters()->find(name, smtk::attribute::SearchStyle::RECURSIVE_ACTIVE);
    auto targetItem =
      pythonOp->parameters()->find(name, smtk::attribute::SearchStyle::RECURSIVE_ACTIVE);
    if (sourceItem != nullptr && targetItem != nullptr)
    {
      targetItem->assign(sourceItem);
    }
  }

  // Set resource items
  auto modelSet = project->resources().findByRole<smtk::model::Resource>("analysis-mesh");
  checkMacro(!modelSet.empty(), "Internal Error: no analysis mesh");
  checkMacro(
    modelSet.size() == 1, "More than 1 analyis mesh (" << modelSet.size() << ") - cannot assign");
  smtk::model::ResourcePtr modelResource = *(modelSet.begin());

  auto resItem = pythonOp->parameters()->findResource("model");
  checkMacro(resItem != nullptr, "Internal Error: resource item for model not found in python op");
  bool didSetValue = resItem->setValue(modelResource);
  checkMacro(didSetValue, "Internal Error: failed to set model resource on python op");

  smtk::resource::ResourcePtr attResource = this->parameters()->findResource("analysis")->value();
  checkMacro(attResource != nullptr, "Internal Erro: analysis resource not found in parameters.");
  didSetValue = pythonOp->parameters()->findResource("attributes")->setValue(attResource);
  checkMacro(didSetValue, "Internal Error: failed to set attribute resource on python op.");

  // Check the submit-to-nersc flag
  smtk::attribute::ConstItemPtr nerscSimItem =
    this->parameters()->find("NERSCSimulation", smtk::attribute::SearchStyle::IMMEDIATE);

  if (nerscSimItem && nerscSimItem->isEnabled())
  {
    auto pyItem =
      pythonOp->parameters()->find("NERSCSimulation", smtk::attribute::SearchStyle::IMMEDIATE);
    pyItem->assign(nerscSimItem);
  }

  bool ready = pythonOp->ableToOperate();
  checkMacro(ready, "Internal Error: python operation return false for ableToOperate().");
  auto pyResult = pythonOp->operate();
  if (pyResult == nullptr)
  {
    std::cout << "WARNING: python export Result is null" << std::endl;
    return this->createResult(smtk::operation::Operation::Outcome::FAILED);
  }

  // Copy the outcome and create result
  int pyOutcome = pyResult->findInt("outcome")->value();
  auto thisOutcome = static_cast<smtk::operation::Operation::Outcome>(pyOutcome);
  auto thisResult = this->createResult(thisOutcome);

  // Copy log messages from the python op
  logger.append(pythonOp->log());

  // Copy the output folder
  std::string name = "OutputFile";
  auto folderItem = pyResult->findFile(name);
  if (folderItem && folderItem->isSet())
  {
    thisResult->findFile(name)->setValue(folderItem->value());
#ifndef NDEBUG
    std::cout << "Python export returned " << name << " = " << folderItem->value() << std::endl;
#endif
  }

  // Copy string items and save job id
  std::string cumulusJobId;
  std::vector<std::string> stringItemNames = {
    "CumulusJobId", "CumulusOutputFolder", "NerscJobFolder", "NerscInputFolder"
  };
  for (const auto& name : stringItemNames)
  {
    auto pyItem = pyResult->findString(name);
    if (pyItem && pyItem->isEnabled())
    {
      thisResult->findString(name)->setIsEnabled(true);
      thisResult->findString(name)->setValue(pyItem->value());
      if (name == "CumulusJobId")
      {
        cumulusJobId = pyItem->value();
      }
#ifndef NDEBUG
      std::cout << "Python export returned " << name << " = " << pyItem->value() << std::endl;
#endif
    }
  }

  // Unregister python operation
  this->manager()->unregisterOperation(operationName);

  // If job submitted, notify project
  if (!cumulusJobId.empty())
  {
    bool ok = project->onJobSubmit(this->parameters(), thisResult, logger);
    checkMacro(ok, "Error storing job record");
  }

  // Done
  return thisResult;
}

smtk::operation::Operation::Specification Export::createSpecification()
{
  Specification spec = this->smtk::operation::XMLOperation::createSpecification();

  // Special logic to copy GroupItem named "NERSCSimulation"
  // from "ace3p-submit" attribute defintion to "ace3p-export".
  // This so that we can reuse submit settings in both python
  // and C++ export operations.
  auto exportDefn = spec->findDefinition("ace3p-export");
  if (exportDefn == nullptr)
  {
    smtkErrorMacro(
      this->log(), "Internal Error: failed to find \"ace3p-export\" attribute definition.");
    return spec;
  }

  auto submitDefn = spec->findDefinition("ace3p-submit");
  auto nerscItemDefn = submitDefn->itemDefinition(0);
  if (nerscItemDefn == nullptr)
  {
    smtkErrorMacro(
      this->log(), "Internal Error: failed to find \"ace3p-submit\" attribute definition.");
    return spec;
  }
  submitDefn->removeItemDefinition(nerscItemDefn);
  bool added = exportDefn->addItemDefinition(nerscItemDefn);
  if (!added)
  {
    smtkErrorMacro(
      this->log(), "INTERNAL ERROR: Failed to add NERSCSimulation item to export definition");
  }
  return spec;
}

const char* Export::xmlDescription() const
{
  return Export_xml;
}
} // namespace ace3p
} // namespace simulation
} // namespace smtk
