<?xml version="1.0" encoding="utf-8" ?>
<!-- Description of the ACE3P Project "Create" Operation -->
<SMTK_AttributeResource Version="4">
  <Definitions>
    <include href="smtk/operation/Operation.xml"/>
    <!-- Parameters -->
    <AttDef Type="newAnalysis" Label="Project - New Analysis" BaseType="operation">
      <BriefDescription>
        Create a new Analysis (Attribute Resource) within a project.
      </BriefDescription>


      <AssociationsDef Name="project" Label="Project" NumberOfRequiredValues="1"
                       Extensible="false" OnlyResources="true"
                       AdvanceLevel="1">
        <Accepts>
          <Resource Name="smtk::simulation::ace3p::Project"/>
          <!-- Accept *any* project as temp workaround -->
          <Resource Name="smtk::project::Project"/>
        </Accepts>
      </AssociationsDef>


      <ItemDefinitions>
        <String Name="analysis-name" Label="Analysis Name">
          <BriefDescription>
            The label used to identify the analysis-specification resource.
          </BriefDescription>
          <DetailedDescription>
            The label used to identify the analysis-specification resource.
            This is for convenience when multiple analyses are included in
            the same project.
          </DetailedDescription>
          <DefaultValue>Analysis</DefaultValue>
        </String>

        <File Name="simulation-template" Label="Override Simulation Template"
          AdvanceLevel="1" ShouldExist="true" Optional="true"
          IsEnabledByDefault="false"
          FileFilters="CMB Template Files (*.sbt);;All Files (*)">
          <BriefDescription>
            The simulation template to use in place of the default.
          </BriefDescription>
        </File>

        <!-- logic for picking between a new mesh loaded
             from file or a mesh already living in the project -->
        <String Name="assign-mesh" Label="Select Mesh Origin">
          <ChildrenDefinitions>
            <File Name="analysis-mesh-file" Label="Analysis Mesh File" NumberOfRequiredValues="1"
              FileFilters="Exodus Files (*.ex? *.gen);;NetCDF Files (*.ncdf);;All Files (*)">
              <BriefDescription>
                The input file to be used as the mesh.
              </BriefDescription>
            </File>
            <Void Name="copy-file" Label="Copy Geometry File(s) Into Project Folder" AdvanceLevel="1" IsEnabledByDefault="true">
              <BriefDescription>If enabled, store a copy of the geometry file in the project directory.</BriefDescription>
            </Void>
            <Resource Name="existing-analysis-mesh">
              <Accepts>
                <Resource Name="smtk::model::Resource" />
              </Accepts>
            </Resource>
          </ChildrenDefinitions>
          <DiscreteInfo>
            <Structure>
              <Value Enum="Open New Mesh From File">open-new-mesh</Value>
              <Items>
                <Item>analysis-mesh-file</Item>
                <Item>copy-file</Item>
              </Items>
            </Structure>
            <Structure>
              <Value Enum="Link to Existing Mesh">link-existing-mesh</Value>
              <Items>
                <Item>existing-analysis-mesh</Item>
              </Items>
            </Structure>
          </DiscreteInfo>
        </String>


      </ItemDefinitions>
    </AttDef>
    <!-- Result -->
    <include href="smtk/operation/Result.xml"/>
    <AttDef Type="result(newAnalysis)" BaseType="result">
      <ItemDefinitions>

        <Resource Name="resource" HoldReference="true">
          <Accepts>
            <Resource Name="smtk::simulation::ace3p::Project"/>
          </Accepts>
        </Resource>
      </ItemDefinitions>
    </AttDef>
  </Definitions>

  <Views>
    <View Type="Operation" Title="New Analysis" TopLevel="true" FilterByAdvanceLevel="true" FilterByCategory="false">
      <InstancedAttributes>
        <Att Name="newAnalysis" Type="newAnalysis"></Att>
      </InstancedAttributes>
    </View>
  </Views>
</SMTK_AttributeResource>
