//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_Project_h
#define smtk_simulation_ace3p_Project_h

#include "smtk/simulation/ace3p/Exports.h"
#include "smtk/simulation/ace3p/JobsManifest.h"

#include "smtk/PublicPointerDefs.h"
#include "smtk/project/Project.h"

#include "smtk/simulation/ace3p/DerivedFrom.h"
#include "smtk/simulation/ace3p/Exports.h"

#include "smtk/PublicPointerDefs.h"

#include "smtk/io/Logger.h"
#include "smtk/project/Project.h"

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class SMTKACE3P_EXPORT Project // : public smtk::project::Project
  : public DerivedFrom<Project, smtk::project::Project>
{
public:
  smtkTypeMacro(smtk::simulation::ace3p::Project);
  smtkCreateMacro(smtk::project::Project);
  smtkSharedFromThisMacro(smtk::project::Project);

  virtual ~Project() {}

  // add a Job Record to the Jobs Manifest
  void addJobRecord(nlohmann::json job);

  // write data to a job record field
  void setJobRecordField(int idx, std::string key, std::string value);

  void getJobRecordField(int idx, std::string key, std::string& value);

  // read a Jobs Manifest from file
  bool readJobsManifest();

  // Write current manifest to file
  bool writeJobsManifest() const;

  // get the jobs Manifest
  const JobsManifest& jobsManifest() const { return m_jobsManifest; }
  std::string jobData(int idx, std::string key);
  bool onJobSubmit(
    smtk::attribute::AttributePtr exportSpec,
    smtk::attribute::AttributePtr exportResult,
    smtk::io::Logger& logger);

protected:
  Project();

private:
  JobsManifest m_jobsManifest;
  // std::vector<smtk::common::UUID> m_analysis_order;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
