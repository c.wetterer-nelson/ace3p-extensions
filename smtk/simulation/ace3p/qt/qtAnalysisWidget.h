//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtAnalysisWidget
#define smtk_simulation_ace3p_qt_qtAnalysisWidget

#include "smtk/simulation/ace3p/qt/Exports.h"

#include <QWidget>

#include "smtk/PublicPointerDefs.h"

namespace Ui
{
class qtAnalysisWidget;
}

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class qtAnalysisModel;
class qtResourceModel;
class qtJobsModel;

class SMTKACE3PQTEXT_EXPORT qtAnalysisWidget : public QWidget
{
  Q_OBJECT

public:
  qtAnalysisWidget(QWidget* parentWidget = nullptr);
  ~qtAnalysisWidget() = default;

signals:
  void requestNewAnalysis();

public slots:
  void onProjectLoaded(smtk::project::ProjectPtr);
  void onAnalysesUpdated();
  void onNewAnalysisPressed() { emit requestNewAnalysis(); };
  void onJobAdded(const QString& cumulusJobId);

protected slots:

protected:
  // Qt model for the analysis table
  qtAnalysisModel* m_analysis_model;

  // Qt model for the resource table
  qtResourceModel* m_resource_model;

  // qt model for the jobs table
  qtJobsModel* m_jobs_model;

private:
  Ui::qtAnalysisWidget* ui;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
