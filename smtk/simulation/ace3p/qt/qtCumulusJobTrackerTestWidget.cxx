//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/ace3p/qt/qtCumulusJobTrackerTestWidget.h"

#include "smtk/cumulus/jobspanel/cumulusproxy.h"
#include "smtk/newt/qtNewtInterface.h"
#include "smtk/simulation/ace3p/qt/qtCumulusJobTracker.h"

#include "smtk/simulation/ace3p/qt/ui_qtCumulusJobTrackerTestWidget.h"

#include <QDebug>
#include <QMessageBox>
#include <QString>
#include <QTextStream>

namespace smtk
{
namespace simulation
{
namespace ace3p
{

qtCumulusJobTrackerTestWidget::qtCumulusJobTrackerTestWidget(QWidget* parentWidget)
  : QWidget(parentWidget)
  , m_jobTracker(new qtCumulusJobTracker(this))
  , ui(new Ui::qtCumulusJobTrackerTestWidget)
{
  this->ui->setupUi(this);

  m_jobTracker->setPollingIntervalSeconds(5);

  QObject::connect(
    this->ui->m_loginButton,
    &QPushButton::clicked,
    this,
    &qtCumulusJobTrackerTestWidget::onLoginTrigger);
  QObject::connect(
    this->ui->m_loadJobsButton,
    &QPushButton::clicked,
    this,
    &qtCumulusJobTrackerTestWidget::onLoadJobsTrigger);
  QObject::connect(
    this->ui->m_pollOnceButton,
    &QPushButton::clicked,
    this,
    &qtCumulusJobTrackerTestWidget::onPollOnceTrigger);
  QObject::connect(
    this->ui->m_pollContinuousCheckBox,
    &QCheckBox::toggled,
    this,
    &qtCumulusJobTrackerTestWidget::onPollContinuousToggled);

  QObject::connect(
    m_jobTracker,
    &qtCumulusJobTracker::jobStatus,
    this,
    &qtCumulusJobTrackerTestWidget::onJobStatus);
  QObject::connect(m_jobTracker, &qtCumulusJobTracker::pollingStateChanged, [this](bool polling) {
    QString text = polling ? "ON" : "Off";
    this->ui->m_pollingStateLabel->setText(text);
  });
}

void qtCumulusJobTrackerTestWidget::onJobStatus(
  const QString& cumulusJobId,
  const QString& status,
  const QString& slurmJobId)
{
  QString message;
  QTextStream qs(&message);
  qs << "job status for " << cumulusJobId << ", status " << status << ", slurmJobId " << slurmJobId
     << ".";
  this->ui->m_listWidget->addItem(message);
}

void qtCumulusJobTrackerTestWidget::onNewtLoginComplete(const QString& userName)
{
  QString message;
  QTextStream qs(&message);
  qs << "Logged in as user " << userName << ".";
  this->ui->m_listWidget->addItem(message);

  // Next log into girder
  auto cumulus = cumulus::CumulusProxy::instance();
  QObject::connect(cumulus.get(), &cumulus::CumulusProxy::authenticationFinished, [this]() {
    this->ui->m_listWidget->addItem("Girder authentication finished.");
  });
  cumulus->girderUrl("https://ace3p.kitware.com/api/v1");
  QString newtSessionId = this->ui->m_sessionId->text();
  cumulus->authenticateGirder(newtSessionId);
}

void qtCumulusJobTrackerTestWidget::onLoadJobsTrigger()
{
  m_jobTracker->clear();
  int count = 0;

  QString id1 = this->ui->m_jobId1->text();
  if (!id1.isEmpty())
  {
    m_jobTracker->addJob(id1);
    ++count;
  }

  QString id2 = this->ui->m_jobId2->text();
  if (!id2.isEmpty())
  {
    m_jobTracker->addJob(id2);
    ++count;
  }

  QString message;
  QTextStream qs(&message);
  qs << "Number of jobs loaded " << count << ".";
  this->ui->m_listWidget->addItem(message);
}

void qtCumulusJobTrackerTestWidget::onLoginTrigger()
{
  // Check input fields
  QString userName = this->ui->m_userName->text();
  if (userName.isEmpty())
  {
    this->ui->m_listWidget->addItem("Cannot login: User name not specified");
    return;
  }

  QString newtSessionId = this->ui->m_sessionId->text();
  if (newtSessionId.isEmpty())
  {
    this->ui->m_listWidget->addItem("Cannot login: NEWT session id not specified");
    return;
  }

  auto* newt = newt::qtNewtInterface::instance();
  QObject::connect(
    newt,
    &newt::qtNewtInterface::loginComplete,
    this,
    &qtCumulusJobTrackerTestWidget::onNewtLoginComplete);
  newt->mockLogin(userName, newtSessionId);
}

void qtCumulusJobTrackerTestWidget::onPollContinuousToggled(bool checked)
{
  bool started = m_jobTracker->enablePolling(checked);
  qDebug() << "Polling started?" << started;
}

void qtCumulusJobTrackerTestWidget::onPollOnceTrigger()
{
  bool started = m_jobTracker->pollOnce();
  qDebug() << "Started poll? " << started;
  if (!started)
  {
    this->ui->m_listWidget->addItem("Did NOT poll server; check console for more info.");
  }
}

} // namespace ace3p
} // namespace simulation
} // namespace smtk
