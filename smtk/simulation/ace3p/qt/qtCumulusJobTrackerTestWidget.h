//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtCumulusJobTrackerTestWidget
#define smtk_simulation_ace3p_qt_qtCumulusJobTrackerTestWidget

#include "smtk/simulation/ace3p/qt/Exports.h"

#include <QWidget>

#include "smtk/PublicPointerDefs.h"

namespace Ui
{
class qtCumulusJobTrackerTestWidget;
}

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class qtCumulusJobTracker;

/* \brief Widget for standalong testing of qtCumulusJobTracker
 *
 * For internal testing.
 */
class SMTKACE3PQTEXT_EXPORT qtCumulusJobTrackerTestWidget : public QWidget
{
  Q_OBJECT

public:
  qtCumulusJobTrackerTestWidget(QWidget* parentWidget = nullptr);
  ~qtCumulusJobTrackerTestWidget() = default;

public slots:

protected slots:
  void onJobStatus(const QString& cumulusJobId, const QString& status, const QString& slurmJobId);
  void onNewtLoginComplete(const QString& userName);
  void onLoginTrigger();
  void onLoadJobsTrigger();
  void onPollContinuousToggled(bool checked);
  void onPollOnceTrigger();

protected:
  qtCumulusJobTracker* m_jobTracker;

private:
  Ui::qtCumulusJobTrackerTestWidget* ui;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
