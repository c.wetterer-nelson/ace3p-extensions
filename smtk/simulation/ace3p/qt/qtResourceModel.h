//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef __qtResourceModel_h
#define __qtResourceModel_h

// Qt includes
#include <QAbstractTableModel>

// local includes
#include "smtk/project/Project.h"

class QStringList;

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class qtResourceModel : public QAbstractTableModel
{
  Q_OBJECT
  using Superclass = QAbstractTableModel;

public:
  qtResourceModel(QWidget* parent);
  virtual ~qtResourceModel() = default;

  // reimplemented QtAbstractTableModel member functions
  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole)
    const override;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;

signals:

public slots:
  void populateResources(const smtk::project::ProjectPtr project);
  void updateResources(smtk::resource::ResourcePtr att = nullptr);

protected:
private:
  // column headers on the table
  const QStringList m_headers = { "Type", "Name", "Description", "UUID" };

  const std::string m_defaultDescp = "Write your description of this resource.";
  const int m_textAlignmentRoles[4] = { Qt::AlignCenter,
                                        Qt::AlignCenter,
                                        Qt::AlignVCenter | Qt::AlignJustify,
                                        Qt::AlignCenter };
  // number of columns in the table
  const int m_nCols = 4;

  // number of rows in the table
  int m_nRows = 0;

  // @brief list of analyses tracked by model
  std::vector<smtk::resource::ResourcePtr> m_resources;

  // @brief placeholder text description strings
  // @note (CURRENTLY VOLITILE, TO DO: serialize into Attribute resource)
  std::vector<std::string> m_descriptions;

  // @brief pointer to the current project
  smtk::project::ProjectPtr m_project;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk
#endif
