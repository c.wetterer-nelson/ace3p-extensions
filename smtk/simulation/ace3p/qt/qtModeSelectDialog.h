//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtModeSelectDialog
#define smtk_simulation_ace3p_qt_qtModeSelectDialog

#include "smtk/simulation/ace3p/qt/Exports.h"

#include <QDialog>

namespace Ui
{
class qtModeSelectDialog;
}

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class SMTKACE3PQTEXT_EXPORT qtModeSelectDialog : public QDialog
{
  Q_OBJECT

public:
  qtModeSelectDialog(QWidget* parentWidget = nullptr);
  ~qtModeSelectDialog() = default;

  void setJobDir(const QString& jobDir);
signals:
  void selectedModeFiles(const QString& jobDir, const QStringList& modeFiles);

public slots:
  // @brief initializes the table with a collection of mode files
  void initTable(const QStringList& modeFiles);

protected slots:
  // @brief check if the user has made a valid selection, activate OK
  void checkSelection();

  void accept() override;

private:
  // @brief pointer to UI information
  Ui::qtModeSelectDialog* ui;

  QString m_jobDir;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk
#endif
