//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtJobsWidget
#define smtk_simulation_ace3p_qt_qtJobsWidget

#include "smtk/simulation/ace3p/qt/Exports.h"

#include <QWidget>

#include "smtk/PublicPointerDefs.h"

class QItemSelection;

namespace Ui
{
class qtJobsWidget;
}

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class qtJobsModel;

class SMTKACE3PQTEXT_EXPORT qtJobsWidget : public QWidget
{
  Q_OBJECT

public:
  qtJobsWidget(QWidget* parentWidget = nullptr);
  ~qtJobsWidget() = default;

signals:
  // @brief emitted when download remote button is clicked
  void requestDownloadRemote(const QString& dir);

  // @brief emitted when a navigate button is clicked
  void requestNavigateDir(const QString& dir);

  // @brief emmited when job load button is clicked
  void requestLoadJob(const QString& dir);

public slots:
  // @brief set pointer to current project
  void setProject(smtk::project::ProjectPtr project);

  // @brief toggle the visibility of the jobs details panel
  void toggleDetailsVisibility(const QItemSelection& selected, const QItemSelection& deselected);

  // @brief slot called when job is created
  void onJobAdded(const QString& cumulusJobId);

  // @brief update the job name on manifest
  void jobNameChanged(const QString& text);

  void notesChanged();

  // @brief call when the project is closed by the app (reset widget)
  void onProjectClosed();

  // @brief connect to navigate remote button pressed signal
  void onNavigateRemoteClicked();

  // @brief connect to navigate input button pressed signal
  void onNavigateInputClicked();

  // @brief request the plugin to load the job's data into paraview
  void onLoadJobClicked();

  // @brief request folder download from Cumulus proxy
  void downloadJob();
protected slots:
  void pollingCheckBoxStateChanged(bool checked);

protected:
  // Enables polling widget if signed in and jobs exists.
  // Returns true if widgets are enabled.
  bool enablePollingWidgets();

  // Enables job detail shortcut buttons connected to NERSC browser
  // Except the slactools button which depends on download state
  void enableJobToolButtons(bool enable = true);

  void onJobDownloaded(const QString& jobId);

private:
  // @brief pointer to UI information
  Ui::qtJobsWidget* ui;

  // @brief pointer to jobs table model
  qtJobsModel* m_jobs_model;
};

} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
