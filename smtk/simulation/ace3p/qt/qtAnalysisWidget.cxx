//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/simulation/ace3p/qt/qtAnalysisWidget.h"
#include "smtk/simulation/ace3p/qt/ui_qtAnalysisWidget.h"

#include "smtk/simulation/ace3p/qt/qtAnalysisModel.h"
#include "smtk/simulation/ace3p/qt/qtJobsModel.h"
#include "smtk/simulation/ace3p/qt/qtResourceModel.h"

namespace smtk
{
namespace simulation
{
namespace ace3p
{

qtAnalysisWidget::qtAnalysisWidget(QWidget* parentWidget)
  : QWidget(parentWidget)
  , ui(new Ui::qtAnalysisWidget)
  , m_analysis_model(new qtAnalysisModel(this))
  , m_resource_model(new qtResourceModel(this))
  , m_jobs_model(new qtJobsModel(this))
{
  this->ui->setupUi(this);

  QObject::connect(
    this->ui->m_newAnalysis, &QPushButton::pressed, this, &qtAnalysisWidget::onNewAnalysisPressed);

  this->ui->m_newAnalysis->hide();
  this->ui->m_copyAnalysis->hide();

  this->ui->m_analysisTable->setModel(m_analysis_model);
  this->ui->m_analysisTable->setSelectionBehavior(QAbstractItemView::SelectRows);
  this->ui->m_analysisTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  this->ui->m_analysisTable->setEditTriggers(QAbstractItemView::DoubleClicked);
  this->ui->m_analysisTable->setWordWrap(true);

  // Todo there is some way to make the "Description" column use any remaining space
  this->ui->m_analysisTable->resizeColumnsToContents();
  // connect(this->ui->m_analysisTable, SIGNAL(doubleClicked(const QModelIndex&)),
  //         this->ui->m_analysisTable, SLOT(edit(const QModelIndex&)));
  // QObject::connect(
  //   this->ui->m_analysisTable, &QTableView::clicked,
  //   this->m_analysis_model,    &qtAnalysisModel::onAnalysisSelected
  // );

  this->ui->m_analysisTable->hide();
  QObject::connect(
    this->ui->m_analysisTable->selectionModel(),
    &QItemSelectionModel::selectionChanged,
    this->m_analysis_model,
    &qtAnalysisModel::analysisSelectionChanged);

  this->ui->m_resourceTable->setModel(m_resource_model);
  this->ui->m_resourceTable->setSelectionBehavior(QAbstractItemView::SelectRows);
  this->ui->m_resourceTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  this->ui->m_resourceTable->setEditTriggers(QAbstractItemView::DoubleClicked);
  this->ui->m_resourceTable->setWordWrap(true);
  this->ui->m_resourceTable->hide();

  this->ui->m_jobsTable->setModel(m_jobs_model);
  this->ui->m_jobsTable->setSelectionBehavior(QAbstractItemView::SelectRows);
  this->ui->m_jobsTable->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
  this->ui->m_jobsTable->setEditTriggers(QAbstractItemView::DoubleClicked);
  this->ui->m_jobsTable->setWordWrap(true);

  // connect selected analysis to filter resource table
  QObject::connect(
    this->m_analysis_model,
    &qtAnalysisModel::selectedAnalysis,
    this->m_resource_model,
    &qtResourceModel::updateResources);
}

void qtAnalysisWidget::onProjectLoaded(smtk::project::ProjectPtr project)
{
  m_analysis_model->populateAnalyses(project);
  m_resource_model->populateResources(project);
  m_jobs_model->populateJobs(project);
}

void qtAnalysisWidget::onAnalysesUpdated()
{
  m_analysis_model->updateAnalyses();
}

void qtAnalysisWidget::onJobAdded(const QString& cumulusJobId)
{
  m_jobs_model->onJobAdded(cumulusJobId);
}

} // namespace ace3p
} // namespace simulation
} // namespace smtk
