//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================
#ifndef smtk_simulation_ace3p_qt_qtAnalysisModel_h
#define smtk_simulation_ace3p_qt_qtAnalysisModel_h

#include "smtk/simulation/ace3p/qt/Exports.h"

// Qt includes
#include <QAbstractTableModel>
#include <QItemSelectionModel>

// smtk includes
#include "smtk/PublicPointerDefs.h"

class QStringList;

namespace smtk
{
namespace simulation
{
namespace ace3p
{

class SMTKACE3PQTEXT_EXPORT qtAnalysisModel : public QAbstractTableModel
{
  Q_OBJECT
  using Superclass = QAbstractTableModel;

public:
  qtAnalysisModel(QWidget* parent);
  virtual ~qtAnalysisModel() = default;

  int rowCount(const QModelIndex& parent = QModelIndex()) const override;
  int columnCount(const QModelIndex& parent = QModelIndex()) const override;
  QVariant data(const QModelIndex& index, int role = Qt::DisplayRole) const override;
  QVariant headerData(int section, Qt::Orientation orientation, int role = Qt::DisplayRole)
    const override;
  bool setData(const QModelIndex& index, const QVariant& value, int role = Qt::EditRole) override;
  Qt::ItemFlags flags(const QModelIndex& index) const override;
signals:
  void selectedAnalysis(const smtk::resource::ResourcePtr);

public slots:
  void populateAnalyses(const smtk::project::ProjectPtr project);
  void updateAnalyses();
  void analysisSelectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
  void onAnalysisSelected(const QModelIndex& index);

protected:
private:
  // column headers on the table
  const QStringList m_headers = { "Name", "ACE3P Code", "Description", "UUID" };

  const std::string m_defaultDescp = "Write your description of this analysis.";
  const int m_textAlignmentRoles[4] = { Qt::AlignCenter,
                                        Qt::AlignCenter,
                                        Qt::AlignVCenter | Qt::AlignJustify,
                                        Qt::AlignCenter };
  // number of columns in the table
  const int m_nCols = 4;

  // number of rows in the table
  int m_nRows = 0;

  // @brief list of analyses tracked by model
  std::vector<smtk::resource::ResourcePtr> m_analyses;

  // @brief placeholder text description strings
  // @note (CURRENTLY VOLITILE, TO DO: serialize into Attribute resource)
  std::vector<std::string> m_descriptions;

  // @brief pointer to the current project
  smtk::project::ProjectPtr m_project;
};
} // namespace ace3p
} // namespace simulation
} // namespace smtk

#endif
