//=========================================================================
//  Copyright (c) Kitware, Inc.
//  All rights reserved.
//  See LICENSE.txt for details.
//
//  This software is distributed WITHOUT ANY WARRANTY; without even
//  the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
//  PURPOSE.  See the above copyright notice for more information.
//=========================================================================

#include "smtk/newt/qtApplicationLoginSupport.h"
#include "smtk/simulation/ace3p/qt/qtCumulusJobTrackerTestWidget.h"

#include <QApplication>

#include <iostream>

int main(int argc, char* argv[])
{
  QApplication app(argc, argv);

  newt::qtApplicationLoginSupport support;
  if (!support.checkSSL())
  {
    return -1;
  }

  // Display the widget
  auto* w = new smtk::simulation::ace3p::qtCumulusJobTrackerTestWidget();
  w->show();

  int retval = app.exec();
  return retval;
}
